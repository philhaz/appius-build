var express = require('express');
var app = express();

app.use(express.static(__dirname));

app.use(function errorHandler(err, req, res, next) {
    console.error(err);
    res.status(500).send(err.stack);
});

var server = app.listen(8081, function() {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});
