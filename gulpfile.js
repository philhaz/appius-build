var path = require('path');
var gulp = require('gulp-help')(require('gulp'), {
  hideEmpty: true,
  hideDepsMessage: true
});
var argv = require('yargs').argv;
var bower = require('bower');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var $ = require('gulp-load-plugins')({
  rename: {
    'gulp-if': 'gulpif'
  }
});

var config;
var srcPaths = {};
var defaults = {
  "projectDir": process.cwd(),
  "inputDir": "src",
  "outputDir": "dist",
  "paths": {
    "css": "css",
    "js": "js",
    "fonts": "fonts",
    "icons": "images/icons"
  },
  "vhost": null, /* the local dev domain, for live-reload proxying */
  "dependencies": [], /* a list of bower packages */
  "imports": {} /* installed into the input directory */
};

/*********\
| HELPERS |
\*********/

function bytesToSize(bytes) {
  if (bytes == 0) return '0 B';
  var k = 1024;
  var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return Math.round((bytes / Math.pow(k, i)).toPrecision(3)) + ' ' + sizes[i];
}

function outputDiff(data) {
  var difference = (data.savings > 0) ? 'Saved' : 'Added';
  return data.fileName + ': ' + difference + ' ' + bytesToSize(data.savings);
}

/*******\
| TASKS |
\*******/

gulp.task('default', function(callback) {
  var prompter = $.prompt.prompt({
		type: 'input',
		name: 'config',
		message: "Which config would you like to use? (defaults)"
	}, function(res){
    if (!res.config || res.config.length == 0) {
      config = defaults;
    } else {
      try {
        config = require('./config/' + res.config + '.json');
      } catch(e) {
        console.log("Error: Unrecognised configuration");
        return false;
      }
    }

    config.projectDir = path.resolve(process.cwd(), config.projectDir);
    console.log("Working directory:", config.projectDir);

    srcPaths = {
      styles: [config.paths.css + '/**/*.scss',
               config.paths.css + '/**/*.css',
         '!' + config.paths.css + '/**/_*.scss'],
      scripts: [config.paths.js + '/**/*.js',
          '!' + config.paths.js + '/**/_*.js'],
      icons: [config.paths.icons + '/**/*.svg']
    };

    if (!config.outputPaths) {
      config.outputPaths = config.paths;
    } else {
      config.outputPaths = {
        css: config.outputPaths.css || config.paths.css,
        js: config.outputPaths.js || config.paths.js,
        icons: config.outputPaths.icons || config.paths.icons
      };
    }

    runSequence('task-prompt', callback);
	});

  prompter.write();
  return prompter;
});

gulp.task('task-prompt', function(callback) {
  var prompter = $.prompt.prompt({
    type: 'input',
		name: 'task',
		message: "What would you like to do? (monitor)"
  }, function(res) {
    if (!res.task || res.task.length == 0)
      res.task = 'monitor';

    try {
      runSequence(res.task, callback);
    } catch(e) {
      console.log("Error: Unrecognised task");
      return false;
    }
  });

  runSequence('help');

  prompter.write();
  return prompter;
});

gulp.task('fetch-dependencies', 'Download all Bower dependencies', function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  bower.commands
    .install(config.dependencies)
    .on('end', function(installed) {
      console.log("Fetched " + Object.keys(installed).length + " new dependencies");
      callback();
    })
});

gulp.task('install-dependencies', 'Download & import Bower dependencies into working directory', ['fetch-dependencies'], function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  if (!config.imports) config.imports = {}

  for (var row in config.imports) {
    var includeFilter = $.filter(['**/*', '!**/_*'], { restore: true });
    var src = path.resolve(process.cwd(), 'bower_components', row);
    var dest = path.resolve(config.projectDir, config.inputDir, config.imports[row]);

    gulp.src(src, { cwd: config.projectDir })
      .pipe(includeFilter)
      .pipe($.rename({
        prefix: '_' }))
      .pipe(includeFilter.restore)
      .pipe(gulp.dest(dest, { cwd: config.projectDir }));
  }

  callback();
});

gulp.task('clean-styles', 'Remove old styles', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  return gulp.src(config.outputDir + '/' + config.outputPaths.css, { cwd: config.projectDir, read: false })
    .pipe($.clean());
});

gulp.task('clean-scripts', 'Remove old scripts', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  return gulp.src(config.outputDir + '/' + config.outputPaths.js, { cwd: config.projectDir, read: false })
    .pipe($.clean());
});

gulp.task('styles', 'Compile, prefix & minify all SCSS', ['icons'], function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  var sassFilter = $.filter(['**/*.scss'], { restore: true });
  var minFilter = $.filter(['**/*', '!**/*.min.*'], { restore: true });
  var sync = (config.vhost) ? true : false;

  return gulp.src(srcPaths.styles, { cwd: config.projectDir + '/' + config.inputDir, base: config.projectDir + '/' + config.inputDir + '/' + config.paths.css })
    .pipe($.plumber())
    .pipe(minFilter)
    .pipe($.sourcemaps.init())
    .pipe(sassFilter)
    .pipe($.sass({
      'precision': 8,
      includePaths: [config.projectDir + '/' + config.inputDir] })
        .on('error', function(err) {
          $.util.log(err);
          this.emit('end');
        }))
    .pipe(sassFilter.restore)
  .pipe(gulp.dest(config.outputPaths.css, { cwd: config.projectDir + '/' + config.outputDir }))
    .pipe($.rename({
      extname: '.min.css' }))
    .pipe($.bytediff.start())
    .pipe($.cleanCss({ 'compatibility': 'ie7' }))
    .pipe($.autoprefixer({ browsers: ['last 2 version'] }))
    .pipe($.bytediff.stop(outputDiff))
    .pipe($.sourcemaps.write('.', { includeContent: false, sourceRoot: '/' + config.inputDir + '/' + config.outputPaths.css }))
    .pipe(minFilter.restore)
  .pipe(gulp.dest(config.outputPaths.css, { cwd: config.projectDir + '/' + config.outputDir }))
    .pipe($.gulpif(sync, browserSync.stream({ match: '**/*.css' })))
    .on('error', $.util.log);
});

gulp.task('scripts', 'Compile & minify all Javascript', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  var minFilter = $.filter(['**/*', '!**/*.min.*'], { restore: true });

  return gulp.src(srcPaths.scripts, { cwd: config.projectDir + '/' + config.inputDir })
    .pipe($.plumber())
    .pipe(minFilter)
    .pipe($.include())
  .pipe(gulp.dest(config.outputPaths.js, { cwd: config.projectDir + '/' + config.outputDir }))
    .pipe($.sourcemaps.init())
    .pipe($.bytediff.start())
    .pipe($.uglify())
    .pipe($.bytediff.stop(outputDiff))
    .pipe($.rename({
      extname: '.min.js' }))
    .pipe($.sourcemaps.write('.', { includeContent: false, sourceRoot: '/' + config.outputDir + '/' + config.outputPaths.js }))
    .pipe(minFilter.restore)
  .pipe(gulp.dest(config.outputPaths.js, { cwd: config.projectDir + '/' + config.outputDir }))
    .on('error', $.util.log);
});

gulp.task('icons', 'Generate iconfonts from SVG', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  var runTimestamp = Math.round(Date.now() / 1000);
  var fontName = 'icons';

  function glyphmap(glyph) {
    return {
      name: glyph.name.split(' ').join('-'),
      codepoint: glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()
    };
  }

  return gulp.src(srcPaths.icons, { cwd: config.projectDir + '/' + config.inputDir })
    .pipe($.iconfont({
      fontName: fontName,
      formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
      timestamp: runTimestamp
    }))
    .on('glyphs', function(glyphs, options) {
      gulp.src('templates/iconfont.css')
        .pipe($.consolidate('swig', {
          glyphs: glyphs.map(glyphmap),
          fontName: fontName,
          fontPath: '/' + config.outputDir + '/' + config.outputPaths.icons + '/',
          className: 'icon'
        }))
        .pipe($.rename({
          basename: '_icons',
          dirname: 'base',
          extname: '.scss' }))
      .pipe(gulp.dest(config.outputPaths.css, { cwd: config.projectDir + '/' + config.inputDir }))
        .on('error', $.util.log);
    })
  .pipe(gulp.dest(config.outputPaths.fonts, { cwd: config.projectDir + '/' + config.outputDir }))
    .on('error', $.util.log);
});

gulp.task('watch', 'Automatically recompile scripts & styles (long-running)', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  var settings = {
    cwd: config.projectDir + '/' + config.inputDir,
    debounceDelay: 2000
  };

  gulp.watch([config.paths.css + '/**/*.css', config.paths.css + '/**/*.scss'], settings, ['styles']);
  gulp.watch([config.paths.js + '/**/*.js'], settings, ['scripts']);
});

gulp.task('browser-sync', 'Live reload the browser on update (long-running)', function() {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  if (config.vhost) {
    browserSync.init({
      proxy: config.vhost,
      ghostMode: {
        clicks: true,
        forms: true,
        scroll: true
      }
    });
  }
});

gulp.task('install', 'Install project dependencies', function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  runSequence('install-dependencies', callback)
});

gulp.task('clean', 'Remove old assets', function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  runSequence(['clean-styles', 'clean-scripts'], callback)
});

gulp.task('build', 'Recompile styles & scripts', function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  runSequence(['styles', 'scripts'], callback)
});

gulp.task('monitor', 'Build everything & live reload on update (long-running)', function(callback) {
  if (typeof config == 'undefined')
    return console.log('This version does not support launching tasks directly.\n'
                      +'Please run "gulp" to configure');

  runSequence('build', ['watch', 'browser-sync'], callback)
});
